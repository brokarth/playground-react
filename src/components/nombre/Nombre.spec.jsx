import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Nombre from './Nombre'

test('should have a Nombre input', () => {
    const { getByLabelText } = render(<Nombre />);
    const nombreElement = getByLabelText("Nombre");
    expect(nombreElement).toBeInTheDocument();
});

test('should set error if Nombre is empty', () => {
    const { getByText, getByLabelText } = render(<Nombre />);
    const nombreInput = getByLabelText("Nombre");
    const nombreLabel = getByText('Nombre');

    fireEvent.blur(nombreInput);

    expect(nombreLabel).toHaveClass('Mui-error');
});

test('should not set error if Nombre is not empty', () => {
    const { getByText, getByLabelText } = render(<Nombre />);
    const nombreInput = getByLabelText("Nombre");
    const nombreLabel = getByText('Nombre');
    nombreInput.value = 'nombre';

    fireEvent.blur(nombreInput);

    expect(nombreLabel).not.toHaveClass('Mui-error');
});