import React from 'react';
import TextField from '@material-ui/core/TextField';

const Email = () => (<TextField id="email" label="Email" className="form-item" />)

export default Email;