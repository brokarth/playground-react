import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';

const Rut = () => {
    const [error, setError] = useState(false);
    return (
        <TextField
            id="rut"
            label="RUT"
            className="form-item"
            placeholder="11111111k"
            onBlur={(e) => onBlur(e.target, setError)}
            error={error}
        />
    )
}

const onBlur = (t, setError) => {
    if(!t.value) {
        return;
    }
    
    t.value = (t.value || "").replace(/-|\./g, '').split("").reverse().reduce((a, b, c) => (c === 1 ? `${a}-${b}` : c % 3 === 0 ? `${a}${b}.` : `${a}${b}`)).split("").reverse().join("");
    validate(t.value, setError);
};

const validate = (rut = "", setError) => {
    const rutParts = rut.split('-');
    const dv = calculateDV(rutParts[0].replace(/\./g, ''));
    setError(+rutParts[1] !== dv);
};

const calculateDV = (r, c=2) => (11 - (r.split("").reverse().reduce((a, b) => { if (c === 8) c = 2; return a + (b * c++); }, 0) % 11))

export default Rut;
