import React from 'react';

import './App.css';
import Rut from '../components/rut/Rut';
import Nombre from '../components/nombre/Nombre';
import Apellido from '../components/apellido/Apellido';
import FechaNacimiento from '../components/fecha-nacimiento/Fecha-Nacimiento';
import Telefono from '../components/telefono/Telefono';
import Email from '../components/email/Email';

function App() {
  return (
    <form>
      <Rut />
      <Nombre />
      <Apellido />
      <FechaNacimiento />
      <Telefono />
      <Email />
    </form>
  );
}

export default App;