
import React from 'react';
import TextField from '@material-ui/core/TextField';

const FechaNacimiento = () => (<TextField id="fecha-nacimiento" className="form-item" label="Fecha de Nacimiento" type="date" InputLabelProps={{ shrink: true }} />)

export default FechaNacimiento;
