import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';

const Nombre = () => {
    const [error, setError] = useState(false);

    return(
        <TextField
            id="nombre"
            label="Nombre"
            className="form-item"
            placeholder="Nombre"
            onBlur={({ target }) => setError(!target.value)}
            error={error}
        />
    )
}

export default Nombre;