import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Apellido from './Apellido'

test('should have a Apellido input', () => {
    const { getByLabelText } = render(<Apellido />);
    const apellidoElement = getByLabelText("Apellido");
    expect(apellidoElement).toBeInTheDocument();
});

test('should set error if Apellido is empty', () => {
    const { getByText, getByLabelText } = render(<Apellido />);
    const apellidoInput = getByLabelText("Apellido");
    const apellidoLabel = getByText('Apellido');

    fireEvent.blur(apellidoInput);

    expect(apellidoLabel).toHaveClass('Mui-error');
});

test('should not set error if Apellido is not empty', () => {
    const { getByText, getByLabelText } = render(<Apellido />);
    const apellidoInput = getByLabelText("Apellido");
    const apellidoLabel = getByText('Apellido');
    apellidoInput.value = 'apellido';

    fireEvent.blur(apellidoInput);

    expect(apellidoLabel).not.toHaveClass('Mui-error');
});