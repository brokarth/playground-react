import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('should have a rut input', () => {
  const { getByLabelText } = render(<App />);
  const linkElement = getByLabelText("RUT");
  expect(linkElement).toBeInTheDocument();
});

test('should have a name input', () => {
  const { getByLabelText } = render(<App />);
  const linkElement = getByLabelText("Nombre");
  expect(linkElement).toBeInTheDocument();
});

test('should have a last name input', () => {
  const { getByLabelText } = render(<App />);
  const linkElement = getByLabelText("Apellido");
  expect(linkElement).toBeInTheDocument();
});

test('should have a birthdate input', () => {
  const { getByLabelText } = render(<App />);
  const linkElement = getByLabelText("Fecha de Nacimiento");
  expect(linkElement).toBeInTheDocument();
});

test('should have a phone input', () => {
  const { getByLabelText } = render(<App />);
  const linkElement = getByLabelText("Telefono");
  expect(linkElement).toBeInTheDocument();
});

test('should have a email input', () => {
  const { getByLabelText } = render(<App />);
  const linkElement = getByLabelText("Email");
  expect(linkElement).toBeInTheDocument();
});