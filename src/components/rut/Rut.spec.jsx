import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Rut from './Rut'

test('should have a rut input', () => {
    const { getByLabelText } = render(<Rut />);
    const rutElement = getByLabelText("RUT");
    expect(rutElement).toBeInTheDocument();
});

test('should format rut on change', () => {
    const { getByPlaceholderText } = render(<Rut />);
    const rutElement = getByPlaceholderText("11111111k");
    rutElement.value = '111111111';

    fireEvent.blur(rutElement);

    expect(rutElement).toHaveValue('11.111.111-1');
});

test('should set error if rut is invalid', () => {
    const { getByText, getByPlaceholderText } = render(<Rut />);
    const rutiInput = getByPlaceholderText("11111111k");
    const rutLabel = getByText('RUT');
    rutiInput.value = '11111111k';

    fireEvent.blur(rutiInput);

    expect(rutLabel).toHaveClass('Mui-error');
});

test('should not set error if rut is valid', () => {
    const { getByText, getByPlaceholderText } = render(<Rut />);
    const rutiInput = getByPlaceholderText("11111111k");
    const rutLabel = getByText('RUT');
    rutiInput.value = '111111111';

    fireEvent.blur(rutiInput);

    expect(rutLabel).not.toHaveClass('Mui-error');
});

test('should not set error if rut is empty', () => {
    const { getByText, getByPlaceholderText } = render(<Rut />);
    const rutiInput = getByPlaceholderText("11111111k");
    const rutLabel = getByText('RUT');

    fireEvent.blur(rutiInput);

    expect(rutLabel).not.toHaveClass('Mui-error');
});