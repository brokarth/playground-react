import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';

const Apellido = () => {
    const [error, setError] = useState(false);
    return (
        <TextField
            id="apellido"
            label="Apellido"
            className="form-item"
            onBlur={({ target }) => setError(!target.value)}
            error={error}
        />)
}

export default Apellido;