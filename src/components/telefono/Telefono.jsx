import React from 'react';
import TextField from '@material-ui/core/TextField';

const Telefono = () => (<TextField id="telefono" className="form-item" label="Telefono" type="tel"  />)

export default Telefono;